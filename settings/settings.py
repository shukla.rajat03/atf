"""
This module store configuration values for test
"""

# Base URL of the application
BASE_URL = "http://127.0.0.1:8000/"

# URL to fetch all records
LIST_RECORDS_URL = "api/tutorials"

# URL to create records
CREATE_RECORD_URL = "api/tutorials"

# Single DB record
RECORD_INSTANCE_URL = "api/tutorials/(?P<pk>[0-9]+)$"

# DELETE RECORDS
DELETE_RECORDS_URL = "api/tutorials"

# A dictionary for a single record
SINGLE_RECORD = dict({"title": "str", "description": "str", "published": "bool"})

SC_DB_CREATED = 201
