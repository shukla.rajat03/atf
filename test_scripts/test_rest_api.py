"""
The purpose of this module
is to test CRUD operations.
CRUD stands for
C: Create records: Insert record
R: Read records: Read operations on database
U: Update record: Update records
D: Delete record: Delete records or selected records
"""
import logging
import pytest
import random

from common import asserts
from lib import api_helper
from settings import settings

LOGGER = logging.getLogger(__name__)


def test_db_create_operation():
    """
    Test create operations, insert records into database
    """
    url = settings.BASE_URL + settings.LIST_RECORDS_URL
    test_data = api_helper.generate_random_test_record()
    status_code = api_helper.post_record(url, json=test_data)
    asserts.assert_equal(status_code, settings.SC_DB_CREATED)


def test_db_read_operation():
    """
    Test read records from DB
    """
    # Get number of records from database
    # compare the returned records by API
    # with the existing database records
    url = settings.BASE_URL + settings.LIST_RECORDS_URL
    records = api_helper.get_all_records(url)
    print(records)
    assert len(records) > 0
    # TODO: Need to add steps to communicate with database


def test_db_update_operation():
    """
    Test a record is updated.
    """
    url = settings.BASE_URL + settings.LIST_RECORDS_URL
    records = api_helper.get_all_records(url)
    record = random.choice(records)
    id = int(record["id"])
    url += "/{id}".format(id=id)
    test_data = api_helper.generate_random_test_record()
    test_data["id"] = id
    api_helper.update_record(url, json=test_data)
    updated_record = api_helper.get_all_records(url)
    asserts.assert_equal(test_data, updated_record, msg="Record is not updated")
    LOGGER.info("eggs info")
    LOGGER.warning("eggs warning")
    LOGGER.error("eggs error")
    LOGGER.critical("eggs critical")


def test_db_delete_operation():
    """
    Test a record is deleted from database.
    """
    url = settings.BASE_URL + settings.LIST_RECORDS_URL
    api_helper.delete_records(url=url, all_records=True)
    asserts.assert_equal(
        actual=len(api_helper.get_all_records(url)),
        expected=0,
        msg="All records are not deleted",
    )
    # Delete a specific record
    api_helper.post_record(
        url=url, data=None, json=api_helper.generate_random_test_record()
    )
    # Delete a record of specific key ID
    id = api_helper.get_all_records(url)[0]["id"]
    api_helper.delete_records(url=url, key_id=id)
    asserts.assert_equal(
        actual=len(api_helper.get_all_records(url)),
        expected=0,
        msg="All records are not deleted",
    )
    # Delete a record having invalid key ID, expect an error
    with asserts.assert_raises_invalid_record_operation():
        api_helper.delete_records(url, key_id=1)
