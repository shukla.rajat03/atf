"""
This is a helper module.
This module provides helper functions
for different operations
"""
import random
import requests
import string
import url_validator
import json as _json

import boto

from settings import settings
from common import exceptions


def get_all_records(url):
    """
    Return list of records in json format.

    :return: All fetched records
    :rtype: ``dict``
    """
    if not is_valid_url(url):
        raise ValueError("Invalid URL {url} passed in argument".format(url=url))
    try:
        response = requests.get(url)
        # For success response return value
        # else raise error
        if response.status_code == 200:
            return response.json()
        else:
            raise exceptions.InvalidStatusCode(
                "Invalid status code {code}"
                "for get operation, URL is {url}".format(
                    code=response.status_code, url=url
                )
            )
    except exceptions.ConnectionError as e:
        raise exceptions.ConnectionError(
            "Connection error occurred ,"
            "make sure service is running for URL "
            "{url}, raised error is {err}".format(url=url, err=e)
        )


def post_record(url, data=None, json=None, **kwargs):
    """
    Post records to web application

    :param url: URL to post data
    :type url: ``str``
    :param data: The json data
    :type  data: ``dict``
    :return: The response of post command
    """
    if not is_valid_url(url):
        raise ValueError("Invalid URL {url} passed in argument".format(url=url))

    # Validate non empty dictionary is passed in argument
    if not isinstance(json, dict) or len(json) <= 0:
        raise ValueError("Non empty dictionary must be passed as an argument")

    try:
        response = requests.post(url, data, json, **kwargs)
        return response.status_code
    except Exception as e:
        raise exceptions.InvalidStatusCode(
            "Invalid status code {code}".format(response.status_code)
        )


def delete_records(url, all_records=True, key_id=None):
    """
    Delete either all records or specific records

    :param all_records: Boolean flag if True delete all records
    :type all_records: ``str``
    :param key_id: Key ID of a record
    :type key_id: ``str``
    :return: Boolean flag else raised error
    :type: ``bool``
    """
    if key_id:
        url += "/{id}".format(id=key_id)
    response = requests.delete(url)
    if response.status_code != 204:
        raise exceptions.InvalidRecordOperation(msg="assertion error")


def update_record(url, data=None, json=None, **kwargs):
    """
    Post records to web application

    :param url: URL to post data
    :type url: ``str``
    :param data: The json data
    :type  data: ``dict``
    :return: The response of post command
    """
    response = None
    if not is_valid_url(url):
        raise ValueError("Invalid URL {url} passed in argument".format(url=url))

    # Validate non empty dictionary is passed in argument
    if not isinstance(json, dict) or len(json) <= 0:
        raise ValueError("Non empty dictionary must be passed as an argument")

    try:
        response = requests.put(url, data=json)
        return response.status_code
    except Exception as e:
        raise exceptions.InvalidStatusCode(
            "Invalid status code {code}".format(response.status_code)
        )


def is_valid_url(url):
    """
    Checks passed URL is valid

    :param url: The URL of application
    :type url: ``str``
    :return: True if URL is valid
    :rtype: ``bool``
    """
    if isinstance(url, str):
        return url_validator.url_validate(url)


def generate_random_test_record():
    """
    Helper function generates random test data
    """
    record = {}
    for key, value in settings.SINGLE_RECORD.items():
        record[key] = get_random_value(data_type=value)
    return record


def get_random_value(data_type):
    """
    Method generate random value according to its type

    :param data_type: String specify data type
    :type data_type: ``str``
    :return: The generated random data
    :rtype: ``str`` or ``bool``
    """
    if eval(data_type) == str:
        return get_random_string(random.randint(5, 60))
    elif eval(data_type) == bool:
        return random.choice([True, False])
    else:
        raise ValueError(
            "This type {typ} is not handled by "
            "get_random_value function".format(typ=data_type)
        )


def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = "".join(random.choice(letters) for i in range(length))
    print("Random string of length", length, "is:", result_str)
    return result_str


class Error(object):
    """
    Just a little class to ducktype the ErrorResult class and its id
    """

    def __init__(self, error_id):
        self.id = error_id
        self.details = "details"
        self.action = "action"


if __name__ == "__main__":
    pass
    b = 2
