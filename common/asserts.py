"""
This module provides different assertion function
"""
from common import exceptions
from contextlib import contextmanager


def _msg(msg, default_msg):
    # If there is a custom message, prepend it to the default, we still
    # want to see what the comparison failures are
    if msg:
        return msg + ": %s" % str(default_msg)
    else:
        return default_msg


def assert_equal(actual, expected, msg=None):
    """
    Validates actual and expected values are same

    :param actual: The actual value
    :param expected: The expected value
    :param msg: The input message
    """
    if not actual == expected:
        msg = _msg(
            msg,
            "Actual value {actual_value} is not same as expected"
            "value {expected_value}".format(
                actual_value=actual, expected_value=expected,
            )
        )
        raise AssertionError(msg)


def assert_true(expr, msg=None):
    """
    Asserts that the given expression is true

    :param expr: The expression to check
    """
    if not expr:
        msg = _msg(msg, "Expected %s to be true" % expr)
        raise exceptions.FrameworkException(msg)


@contextmanager
def assert_raises_invalid_record_operation():
    try:
        yield
    except exceptions.InvalidRecordOperation, e:
        print("InvalidRecordOperation error raised for operation on record")
    except Exception, e:
        print("Expected exception is InvalidRecordOperation and raised "
              "exception is {exp}".format(exp=e))
        raise exceptions.FrameworkException("No exceptions raised")
    else:
        raise exceptions.FrameworkException("No exceptions raised, else block")
