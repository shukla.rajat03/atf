from requests.exceptions import ConnectionError


class FrameworkException(Exception):
    """
    Base exception class for framework
    """

    def __init__(self, msg=None, **kwargs):
        super(FrameworkException, self).__init__()


class ServiceUnavailableError(FrameworkException):
    """
    Connect error from request module
    """

    def __init__(self, msg=None, **kwargs):
        super(ServiceUnavailableError, self).__init__()


class InvalidStatusCode(FrameworkException):
    """
    Invalid status code for a operation
    """

    def __init__(self, msg=None, **kwargs):
        super(InvalidStatusCode, self).__init__()


class AssertionError(FrameworkException):
    """
    Exception for assertion
    """

    def __init__(self, msg=None, **kwargs):
        super(AssertionError, self).__init__()


class InvalidRecordOperation(FrameworkException):
    """
    Operation performed on invalid record
    """

    def __init__(self, msg=None, **kwargs):
        super(InvalidRecordOperation, self).__init__()
